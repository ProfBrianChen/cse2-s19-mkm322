// this program will use the Scanner class to obtain from 
// the user the original cost of the check, the percentage 
// tip they wish to pay, and the number of ways the check 
// will be split. Then determine how much each person in the 
// group needs to spend in order to pay the check.

// this import statment accesses the java library for use of Scanner code
import java.util.Scanner;

//  starts main code
public class Check{
  
  // this is the main method and will be where I write the code
  public static void main(String args[]){
    
    // I create an instance that will take input from STDIN
    Scanner myScanner = new Scanner( System.in );
    
    // prompts the user a statement for orignial cost
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    // accepts the user input for previous question
    double checkCost = myScanner.nextDouble();
    
    // prompts the user a statement for percentage tip as a whole number
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");

    // accepts the user input for previous question
    double tipPercent = myScanner.nextDouble();
    
    // we want to convert the percentage into a decimal value
    tipPercent /= 100;

    // prompts the user a statement for people out for dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    
    // accepts the user input for previous question
    int numPeople = myScanner.nextInt();
    
    // declares the total cost, cost per person, and dollars/dimes/pennies
    double totalCost;
    double costPerPerson;
    int dollars;
    int dimes;
    int pennies;
    
    // calculates the total cost by using checkCost multiplied by 1 plus tipPercent
    totalCost = checkCost * (1 + tipPercent);
    
    // calculates the cost per person by dividing totalCost by numPeople
    costPerPerson = totalCost / numPeople;
    
    // calculates dollars per person
    dollars = (int) costPerPerson;
    
    // calculates dimes per person
    dimes = (int) (costPerPerson * 10) % 10;
    
    // calculates pennies per  person
    pennies = (int) (costPerPerson * 100) % 10;
    
    // prints result
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

    
  }
  
}