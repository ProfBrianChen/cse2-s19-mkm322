public class Cyclometer{
  
  //main method required for every Java program
  
  public static void main(String args[]){
    
    // starting our initial values for calculations
    
    int secsTrip1 = 480;  // time for first trip
    double secsTrip2 = 3220;  // time for second trip
    int countsTrip1 = 1561;  // counter for first trip
    int countsTrip2 = 9037; // counter for second trip
    double wheelDiameter = 27.0;  // this is the measure of wheel in inches
    double PI = 3.14159; // estimated pi
    int feetPerMile = 5280;  // conversion ft/mile
    int inchesPerFoot = 12;   // conversion in/ft
    int secondsPerMinute = 60;  // conversion seconds/minutes
    double distanceTrip1, distanceTrip2, totalDistance;  // declares these as  doubles
    
    // now we print time and counts for trips one and two
    System.out.println("Trip 1 took "+
                       (secsTrip1/secondsPerMinute)+" minutes and had "+
                       countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
                       (secsTrip2/secondsPerMinute)+" minutes and had "+
                       countsTrip2+" counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //distance calculated in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    	//Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

    
  } //end of main method
  
}


