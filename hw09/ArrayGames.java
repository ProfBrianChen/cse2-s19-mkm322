//Maximillian Machado
//Brian Chen
//4.16.2019

// This program uses different methods 
// to apply changes to random arrays
// Two options include insert and shorten.

import java.util.Arrays;
import java.util.Scanner;

public class ArrayGames{
	
	// simple method to generate random size arrays of incrementing by 1
	public static int[] generate(){
		
		int[] list = new int[(int)(Math.random() * 11) + 10];
		
		for(int i = 0; i < list.length; i++){
			list[i] = i + 1;
		}
		
		return list;
		
	}
	
	// method used to print array in set notation
	public static void print(int[] list){
		
		System.out.print("{");
		for(int i = 0; i < list.length; i++){
			System.out.print(list[i]);
			if(i != list.length - 1){
				System.out.print(",");
			}
		}
		System.out.println("}");
		
	}
	
	// insert method merges arrays
	public static int[] insert(int[] list1, int[] list2){
		
		// final array created with sum of lengths
		int[] list3 = new int[list1.length + list2.length];
		
		int insert = (int) (Math.random() * list1.length);
		
		// counter keeps track of list1
		int k = 0;
		
		// for-loop generates insertion
		for(int i = 0; i < list1.length + list2.length; i++){
			
			if(i == insert) { 
				
				// case where the random insertion happens
				for(k = 0; k < list2.length; k++){
					list3[i] = list2[k];
					i++;
				}
				
			}
			
			list3[i] = list1[i-k];
			
			
		}
		
		return list3;
		
	}
	
	public static int[] shorten(int[] list, int index){
		
		// if the index is in range run shorten
		if(list.length > index & index >= 0){
			
			int[] sList = new int[list.length - 1];
			
			//counter that keeps track of current index for sList
			int k = 0;
			
			// for-loop that generates shortened array, sList
			for(int i = 0; i < list.length; i++){
				
				
				if(i == index){
					continue;
				}
				
				sList[k] = list[i];
				
				k++;
			}
			return sList;
		
		}
		else{
			return list;
		}
		
	}
	
	public static void main(String[] args){
		
		// declare arrays to keep for calculations
		int[] input1 = generate();
		int[] input2 = generate();
		
		// random input for shorten method
		int random = (int)(Math.random() * 11);
		String junk = "";
		
		Scanner myScan = new Scanner(System.in);
		
		System.out.println("-----:Welcome to Arrays Games:-----");
		
		// infinite loop to ensure the correct method is picked
		while(true){
			System.out.println("Would you like to \"shorten\" or \"insert\"?");
			
			if(myScan.hasNext("insert")){ // insertion happens here
				System.out.print("Input1: ");
				print(input1);
				System.out.print("Input2: ");
				print(input2);
				System.out.print("Output: ");
				print(insert(input1, input2));
				System.out.println();
				break;
			}
			else if(myScan.hasNext("shorten")){ // shorten happens here
				System.out.print("Input1: ");
				print(input1);
				System.out.println("Input2: " + random);
				System.out.print("Output: ");
				print(shorten(input1, random));
				System.out.println();
				break;
			}
			else{ // user's input is stored in junk variable to reprompt
				System.out.println("ERROR: Invalid Input!");
				junk = myScan.nextLine();
			}
		}
	}
	
	
}