// Maximillian Machado
// Brian Checn HW 7
// Due: 3/26/2019

import java.util.Scanner;

// this program observes string inputs and returns true if the strings only have letters
// this program returns false otherwise

// method type 1 checks a full string, and method type 2 checks a string up to desired amount

public class StringAnalysis{
	
	//this method was define to check String inputs
	public static String inputOne(String prompt){
		
		Scanner myScan = new Scanner(System.in);
		System.out.print(prompt);
		
		// continues to prompt user until the input is a String
		while(!myScan.hasNextLine()) {
			System.out.print(prompt);
			String refresh = myScan.nextLine();
		}
		
		// returns the string to called location
		return myScan.nextLine();
	}
	
	//same method as the previous but instead checks for ints
	public static int inputTwo(String prompt){
		
		Scanner myScan = new Scanner(System.in);
		System.out.print(prompt);
		
		while(!myScan.hasNextInt()) {
			System.out.print(prompt);
			String refresh = myScan.nextLine();
		}
		
		return myScan.nextInt();
	}
	
	//this method reads through a string completely to see if there are unwanted chars
	public static boolean anaStr(String x) {
		
		// length is calculated to tell the for loop when to end
		int length = x.length();
		
		// for loop to test each char for unwanted chars
		for (int i = 0; i < length; i++) {
			if('a' > x.charAt(i) || 'z' < x.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	//this method is similar but he user can tell the for loop when to end
	public static boolean anaStr(String x, int y) {
	
		int length = x.length();
		
		//the addition of the if statement in the for loop dictates when to break given the condition
		for (int i = 0; i < length; i++) {
			if(i == y - 1) { // - 1 because the first letter is i = 0
				break;
			}
			if('a' > x.charAt(i) || 'z' < x.charAt(i)) {
				return false;
			}
		}
		return true;
	
	}
	
	//main method is used to print results and call methods
	public static void main(String[] args) {
		
		System.out.println("\nMethod 1: \n");
		System.out.println(anaStr(inputOne("String = ")));
		System.out.print("\nMethod 2: \n");
		System.out.println(anaStr(inputOne("String = "), inputTwo("Read to = ")));
		
	}
}