// Maximillian Machado
// Brian Checn HW 7
// Due: 3/26/2019

// The program uses different methods to compute the area of different shapes

import java.util.Scanner;

public class Area{
	
	// this method is called when checking to see valid inputs
	public static double input(String prompt) {
		
		// intializes important variables for checking
		Scanner myScan = new Scanner(System.in);		
		String refresh = "";
		double input = 0;
		
		// the user is forced into an infinte loop that only breaks if inputs go through gates
		while(true) {
			
			// prompt is a passed variable that helps generalize method
			System.out.print(prompt);
			
			// gate one is to check if the input is a double
			if(!myScan.hasNextDouble()) {
				System.out.println("    Error: please input valid double!");
				refresh = myScan.nextLine();
				continue;
			}
			
			// gate two checks to see if inputs are postive or 0.
			input = myScan.nextDouble();
			if(input < 0) {
				System.out.println("    Error: please input a positive double!");
				continue;
			}
			return input;
			
		}
		
		
	}
	
	// simple method to calculate area for rectangle
	public static double rectangle(double x, double y) {
		return x * y;
		
	}		

	// simple method to calculate area for triangle
	public static double triangle(double x, double y) {
		return 0.5 * x * y;
		
	}

	// simple method to calculate area for circle
	public static double circle(double x) {
		return 3.1415 * x * x;
		
	}

	public static void main(String[] args) {
		
		// these variables will help to see if the user picks that right shape
		Scanner myScan = new Scanner(System.in);
		double area = 0;
		
		// this series of print lines prompt the user to pick a shape.
		System.out.println("\nWhat area wold you like to calculate?\n");
		System.out.println("Options:   \"rectangle\"   \"triangle\"   \"circle\"\n");
		System.out.print("Shape = ");
		
		// shape is started declared so that I could use .equals() to check responses
		String shape = myScan.nextLine();
		
		
		// user is forced into while loop to see if the correct shape is picked
		while (true) {
			
			// if any of the correct strings are picked then the while loop breaks
			if (shape.equals("rectangle") | shape.equals("triangle") | shape.equals("circle")) {
				break;
			}
			// if the user fails, then the user is prompted again
			System.out.println("    Error: please input a valid shape!");
			System.out.print("Shape = ");
			shape = myScan.nextLine();
		}
		
		// the use of switch statement is to calculate the user's response
		switch(shape) {
			case "rectangle":
			area = rectangle(input("Length = "), input("Width = "));
			break;
			
			case "triangle":
			area = triangle(input("Base = "), input("Height = "));
			break;
			
			case "circle":
			area = circle(input("Radius = "));
			break;
		}
		
		// prints out final result.
		System.out.println("Area = " + area);
	}
}