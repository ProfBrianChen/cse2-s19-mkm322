public class Arithmetic{
  //starts main method

  public static void main(String args[]){

    // initialize values

    int numPants = 3; // number of pairs of pants
    double pantsPrice = 34.98; // cost per pair of pants
    int numShirts = 2; // number of shirts
    double shirtPrice = 24.99; // cost per shirt
    int numBelts = 1; // number of belts
    double beltPrice = 33.99; // cost per belt
    double paSalesTax = 0.06; // the tax rate

    // Calculations

    // finds total cost for pants
    double totalCostOfPants = numPants * pantsPrice;

    // finds total cost for shirts
    double totalCostOfShirts = numShirts * shirtPrice;

    // finds total cost for belts
    double totalCostOfBelts = numBelts * beltPrice;

    // finds total sales tax for pants
    double salesTaxPants = (int) (100 * totalCostOfPants * paSalesTax);
    
    // finds total sales tax for shirts
    double salesTaxShirts = (int) (100 * totalCostOfShirts * paSalesTax);

    // finds total sales tax for belt
    double salesTaxBelts = (int) (100 * totalCostOfBelts * paSalesTax);

    // finds total cost of all items
    double totalCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;

    // finds total sales tax of all items
    double salesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;

    // final total with all items and tax
    double grandTotal = totalCost + salesTax / 100;

    // returns the total cost of pants
    System.out.println("\nPants: Price, Sales tax = $" + totalCostOfPants +
              ", $" + salesTaxPants/100);

    // returns the total cost of sweatshirts
    System.out.println("Sweatshirts: Price, Sales tax = $" + totalCostOfShirts +
              ", $" + salesTaxShirts/100);

    // returns the total cost of belts
    System.out.println("Belts: Price, Sales tax = $" + totalCostOfBelts +
              ", $" + salesTaxBelts/100 + "\n");

    // returns the pre-tax total cost
    System.out.println("Pre-tax Total Cost: $" + totalCost);

    // retruns the total sales tax
    System.out.println("Total Sales Tax: $" + salesTax/100);

    // returns the grand total for
    System.out.println("Grand Total: $" + grandTotal);

  }

}
