public class WelcomeClass{
  
  public static void main(String args[]){
    
    ///prints my signature to terminal window followed by a tweet
    System.out.println("\n  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------\n");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-M--K--M--3--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v\n");
    System.out.println("My name is Maximillian Karl Machado, and I am from Miami,");
    System.out.println("Florida. Moving to PA has been a very unique experience thus");
    System.out.println("far. I am extremely excited to see what will happen within the"); 
    System.out.println("next four years during my stay in Lehigh University. I will be"); 
    System.out.println("going into CSB this semester.\n");

  }

}