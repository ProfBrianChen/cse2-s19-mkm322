//Maximillian Machado
//Finished 4/5/2019
//Brain Chen

//-----------:Details:-----------

// this program runs basic statistical
// analysis on a random set of data

import java.util.Arrays;

public class lab08{
	
	public static void main(String[] args){
		
		//the size of the array is randomly generated
		int size = (int)(Math.random() * 51) + 50;
		
		//the array is constructed with given random size
		int[] array = new int[size];
		
		//Length is calculated
		System.out.println("Length = " + array.length);
		
		//Generates an array with random values to "size"
		for (int i = 0; i < size; i++) {
			array[i] = (int)(Math.random() * 100);
			System.out.println("array[" + i + "] = " + array[i]);
		}
		
		//Series of prints and methods that calculates range, mean, standard deviation, and shuffled array
		System.out.println("Range = " + getRange(array));
		System.out.println("Mean = " + getMean(array));
		System.out.println("Standard Deviation = " + getStdDev(array));
		System.out.println("SHUFFLE!\n------------------------------------");
		shuffle(array);
	}
	
	
	public static int getRange(int[] list){ // method to calculate range
		
		Arrays.sort(list);	// sorts the array into increasing order	
		return list[list.length - 1] - list[0]; // returns range
		
	}
	
	public static double getMean(int[] list){ // method to calculate mean
		
		int sum = 0; 
		for(int i = 0; i < list.length; i++){ // recursion that takes the sum with each iteration
			sum = list[i] + sum;
		}
		
		return (double)sum / list.length; // divide the sum of elements by count of elements to create mean double
		
	}
	
	public static double getStdDev(int[] list){ // method to calculate standard deviation
		
		double mean = getMean(list); // mean is calculated out of loop to save time on calculations
		double sum = 0;
		for(int i = 0; i < list.length; i++){
			
			sum = Math.pow(list[i] - mean, 2) + sum; // this calculation is the numerator inside of the radical in the formula
			
		}
		
		return Math.sqrt(sum / (list.length - 1)); // this finishs th calculation of the standard deviation
		
	}
	
	public static void shuffle(int[] list){ // method to shuffle elements
		
		int[] swap = new int[list.length]; // new array with equal length to the passed array
		
		for (int i = 0; i < list.length; i++) {
			
			int random = (int)(Math.random() * list.length); // random integer is created with each iteration
			
			swap[random] = list[random];// random integer picks a random element to select and temporarily store
			swap[i] = list[i];// swap array holds previous value of current index to temporarily store
			list[i] = swap[random]; // current index is swaped with the random index
			list[random] = swap[i]; // random index is swaped with current
			
			System.out.println("array[" + i + "] = " + list[i]); // prints newly shuffled array
			
		}
		
	}
	
	
}