//Maximillian Machado
//Finished 4/6/2019
//Brian Chen

//-----------:Details:-----------

//this program sorts 10 random chars
//into different alphabetical ordered arrays

import java.util.Arrays;

public class Letters{
	
	public static void main(String[] args){
		
		// "size" is the amount of randomly generated characters
		int size = 10;
		
		// "charArr" is the pointer to the array that holds the random char entries
		char[] charArr = new char[size];
		System.out.print("Random character array: ");
		
		// this for-loop dictates each random entry of "charArr"
		for(int i = 0; i < charArr.length; i++){
			
			// this random double roughly ensures random capital and noncapital letters
			double ranLet = Math.random() * 2;
			
			// depending on the values of "ranLet", the entry will be capital or noncapital
			if(ranLet > 1){
				charArr[i] = (char)( (int)(Math.random() * 25) + 98 );
			}
			else if(ranLet <=1){
				charArr[i] = (char)( (int)(Math.random() * 25) + 66 );
			}
			System.out.print(charArr[i]);
		}
		
		System.out.println();
		
		//finally methods that sort alphabetically are called
		getAtoM(charArr);
		getNtoZ(charArr);
	}
	
	public static char[] getAtoM(char[] list){ // pulls the letters A-M to a new array
		
		Arrays.sort(list); // sorts elements by character values
		char[] holder = new char[list.length]; // temperary array to remove unwanted values
		
		int count = 0; // counter to track empty (i.e ' ') entries
		for(int i = 0; i < list.length; i++){
			
			// checks to see if the entry is in range
			if(list[i] >= 'a' & list[i] <= 'm' || list[i] >= 'A' & list[i] <= 'M'){ 
				holder[i] = list[i];
			}
			// if not then replace entry with ' '
			else{
				holder[i] = ' ';
				count++;
			}
			
		}
		
		// this sort is crucial becaues it pushes all valid entries behind ' '
		Arrays.sort(holder);
		char[] newList = new char[list.length - count]; // counter dictates new length of the sorted array
		
		System.out.print("AtoM characters: ");
		
		for(int i = count; i < holder.length; i++){ // constructs new array with sorted values
			
			newList[i-count] = holder[i];
			System.out.print(newList[i-count]);
			
		}
		System.out.println();
		return newList;
	
	}
	
	public static char[] getNtoZ(char[] list){ // pulls the letters N-Z to a new array
		
		Arrays.sort(list); // sorts elements by character values
		char[] holder = new char[list.length]; // temperary array to remove unwanted values
		
		int count = 0; // counter to track empty (i.e ' ') entries
		for(int i = 0; i < list.length; i++){
			
			// checks to see if the entry is in range
			if(list[i] >= 'n' & list[i] <= 'z' || list[i] >= 'N' & list[i] <= 'Z'){ 
				holder[i] = list[i];
			}
			// if not then replace entry with ' '
			else{
				holder[i] = ' ';
				count++;
			}
			
		}
		
		// this sort is crucial becaues it pushes all valid entries behind ' '
		Arrays.sort(holder);
		char[] newList = new char[list.length - count]; // counter dictates new length of the sorted array
		
		System.out.print("NtoZ characters: ");
		
		for(int i = count; i < holder.length; i++){ // constructs new array with sorted values
			
			newList[i-count] = holder[i];
			System.out.print(newList[i-count]);
			
		}
		System.out.println();
		return newList;
	
	}
}