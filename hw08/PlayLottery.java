//Maximillian Machado
//Finished 4/6/2019
//Brian Chen

//-----------:Details:-----------

//This program plays a lottery game.
//The user must input 5 correct int in range.
//If the inputs are equal in order and numbers to the random array,
//then the user wins!

import java.util.Scanner;

public class PlayLottery{
	
	public static void main(String[] args){
		
		// instance of scanner used to take inputs
		Scanner myScan = new Scanner(System.in);
		
		// instance of array to store user inputs
		int[] user = new int[5];
		
		// this for-loop ensures the user inputs are in range
		for(int i = 0; i < 5; i++){ 
			
			System.out.print("Number " + (i + 1) + ": "); // prints current the requested number
			
			// first condition to check is if the input is an int
			if(!myScan.hasNextInt()){ 
				
				// if it is not the loop restarts to the current i and throws away current scanner
				System.out.println("    ERROR: Input is not an integer!");
				String junk = myScan.nextLine();
				i--;
				continue;
			}
			
			// the int is now stored as an entry so that it can be tested for range
			user[i] = myScan.nextInt();
			
			// this is the range condition. if it fails then the loop reiterates
			if(user[i] > 59 | user[i] < 0) {
				System.out.println("    ERROR: Input is not in range!");
				i--;
				continue;
			}
		}
		
		// correct user array is printed
		System.out.print("Enter 5 numbers between 0 and 59: ");
		System.out.println(user[0] + ", " + user[1] + ", " + user[2] + ", " + user[3] + ", " + user[4]);
		
		// winning numbers array is constructed
		int[] winning = numbersPicked();
		
		// winning numbers array is printed
		System.out.print("The winning numbers are: ");
		
		for(int i = 0; i < 5; i++){
			System.out.print(winning[i]);
			if(i != 4){
				System.out.print(", ");
			}
			else{
				System.out.println();
			}
		}
		
		// the winning condition is checked
		if(userWins(user, winning)){
			System.out.println("You win");
		}
		else{
			System.out.println("You lose");
		}
	}
	
	// method 2 is used to check if the user array is equal to the winning
	public static boolean userWins(int[] user, int[] winning){
		
		// this for-loop evaluates each entry 1 by 1 in order
		for(int i = 0; i < 5; i++){
			if(user[i] != winning[i]){
				return false; // there is an inequality then the user loses
			}
		}
		
		return true; // otherwise, the user wins
		
	}
	
	public static int[] numbersPicked(){
		
		// an instance of array is created
		// this array will create the set of winning numbers
		int[] list = new int[5];
		
		// for-loop to make distinct values for the winning set
		for(int i = 0; i < 5; i++){
			
			// the entry is randimly generated
			list[i] = (int) (Math.random() * 60);
			
			// this for-loop is to test all previous entries of repetition.
			// if there is a failure then the for-loop randoms the entry
			// and regenerates the entry
			for(int k = i; k > 0; k--) {
				if(list[i] == list[k - 1]){
					list[i] = (int) (Math.random() * 60);
					k = i;
				}
			}
			
		}
		
		// the correct winnings is returned
		return list;
		
	}
	
	
}