public class CardGenerator{
  
  public static void main(String[] args){
    
    // I calculate the card number by creating a random int [0,52) and add 1
    // so that that the interval of intergers moves to [1,53) or [1,52]
    int cardNumber = (int) (Math.random() * 52) + 1;
    
    // I then preform modulus to figure out the card value, this will reduce time later
    int cardValue = cardNumber % 13;
    
    // I begin the printing of the string
    System.out.print("You picked the ");
    
    // I create this switch statement using the cardValue to quickly find the card
    switch( cardValue ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValue);
    }
    
    // I use these four if statements to determine the suite of the card.
    if ( 13 >= cardNumber ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumber) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumber) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumber) {
      System.out.println(" of Spades");
    }
    
  }
  
}