// Maximillian Machado
// 3/22/2019
// Lab07

// this program creates a random paragraph

// import used libraries
import java.util.Random;
import java.util.Scanner;

// starts the class
public class lab07{  
  
  // creates the adjective method
  public static String adjective(){
    
	// creates an instance of the random method
	Random ranGen = new Random();
    
	// generates a random int from [0,10)
    int randomInt = ranGen.nextInt(10);
    
	// here is a switch to determine which random adjective
    switch (randomInt) {
      case 0:
        return "epic";
        
      case 1:
        return "sweet";
        
      case 2:
        return "interesting";
        
      case 3:
        return "strange";
        
      case 4:
        return "crazy";
        
      case 5:
        return "boring";
        
      case 6:
        return "thought-inducing";
        
      case 7:
        return "cute";
        
      case 8:
        return "weird";
        
      case 9:
        return "smelly";
    }
      
    return "";
  }
  
  // this method is the same as the previous but generates a subject
  public static String subject() {
    Random ranGen = new Random();
    
    int randomInt = ranGen.nextInt(10);
    
    switch (randomInt) {
      case 0:
        return "dog";
        
      case 1:
        return "foot";
        
      case 2:
        return "wig";
        
      case 3:
        return "ring";
        
      case 4:
        return "sock";
        
      case 5:
        return "nose";
        
      case 6:
        return "cars";
        
      case 7:
        return "bicycle";
        
      case 8:
        return "file";
        
      case 9:
        return "cat";
    }
      
    return "";
  }
  
  // this method is the same as the previous but generates a subject
  public static String verb() {
    Random ranGen = new Random();
    
    int randomInt = ranGen.nextInt(10);
    
    switch (randomInt) {
      case 0:
        return "passed";
        
      case 1:
        return "jumped";
        
      case 2:
        return "slapped";
        
      case 3:
        return "kissed";
        
      case 4:
        return "missed";
        
      case 5:
        return "ate";
        
      case 6:
        return "pushed";
        
      case 7:
        return "pulled";
        
      case 8:
        return "broke";
        
      case 9:
        return "hugged";
    }
   
    return "";
  }
  
  public static String object() {
    Random ranGen = new Random();
    
    int randomInt = ranGen.nextInt(10);
    
    switch (randomInt) {
      case 0:
        return "cart";
        
      case 1:
        return "person";
        
      case 2:
        return "rocket";
        
      case 3:
        return "Nissan";
        
      case 4:
        return "Honda";
        
      case 5:
        return "roommate";
        
      case 6:
        return "pencil";
        
      case 7:
        return "bottle";
        
      case 8:
        return "mac-and-chesse";
        
      case 9:
        return "airplane";
    }
      
    return "";
  }
  
  // this method creates the thesis of the paragraph
  public static String thesis(){
		// the subject is kept so that it can be passed to the other methods
		String subject = subject();
		System.out.println("The " + adjective() + " " + adjective() + " " + subject + " " + verb() + " the " + adjective() + " " + object() + ".\n");
		return subject;
  
	}
    
  // this method is used to create the body paragraph
  public static void action(String subject){
		
		// generates random int to see if there should be pronouns or not
		Random ranGen = new Random();
		int randomInt = ranGen.nextInt(2);
		String resubject = "";
	
		
		switch (randomInt) {
			case 0:
			resubject = "It";
			break;
		
			case 1:
			resubject = subject;
			break;
		}
	
		System.out.println("This " + subject + " was " + adjective() + " " + verb() + " to " + adjective() + " " + object() + ".\n");
		System.out.println(resubject + " used " + object() + " to " + verb() + " " + object() + " at the " + adjective() + " " + object() + ".\n");
	
	}
  
  // this method is to create a conclusion that takes on the subject of the thesis
  public static void conclusion(String subject){
	  
		System.out.println("The " + subject + " " + verb() + " its " + object() + ".\n");
	}
	
  // calls the methods to create a random paragraph
  public static void main(String[] args) {
		
		String subject = thesis();
		action(subject);
		conclusion(subject);
	  
	  
	}
	
}
    