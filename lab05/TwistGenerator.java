//Maximillian Machado
//Brian Chen
//3.1.2019
//lab 6

// This code prints out a simple "twist"

import java.util.Scanner; // imports Scanner methods

public class TwistGenerator{

  public static void main(String[] arg){ // starts main method

    Scanner myScan = new Scanner(System.in);// constructs Scanner type
    
    System.out.print("Length = "); // prompts the user for length
    
    int length = -1; // sets length to a value that will trigger the following:
    
    while (!myScan.hasNextInt() || length < 0) // keeps loop until valid input
    {
      
      if (myScan.hasNextInt()) // if the input is an integer
      {
        length = myScan.nextInt(); // stores integer value
        
        if (length < 0) // checks if it is postive or 0
        {
          System.out.print("Error: Length must be a postive integer!\nLength = ");
        }
        else // if it is valid, then the code continues
        {
          break;
        }
        
      }
      
      if (!myScan.hasNextInt()) // if users put anything but integer
      {
        System.out.print("Error: Enter a valid input. \nLength = "); // returns error
        String check = myScan.next(); // junk variable to clear scanner
      }
      
    }

    // the following for-loops are to construct line by line the twist
    
    // I will explain the first one because the other three are the same idea except with different strings
    
    for(int i = 0; i < length; ++i) // line one: intializes for-loop to end when the increment (++i) gets to be the length of the characters per row
    {
      if (i % 3 == 0) // modulo to check which condition the string is going to be
      {
        System.out.print("\\"); // this produces "\" component
      }
      else if (i % 3 == 1)
      {
        System.out.print(" "); // this produces " " component
      }
      else if (i % 3 == 2)
      {
        System.out.print("/"); // this produces "/" component
      }
      
      if (i + 1 == length) // once the line is complete, start new line
      {
        System.out.print("\n"); // new line
      }
      
    }
    
    for(int i = 0; i < length; ++i) // line two
    {
      if (i % 3 == 0)
      {
        System.out.print(" ");
      }
      else if (i % 3 == 1)
      {
        System.out.print("x");
      }
      else if (i % 3 == 2)
      {
        System.out.print(" ");
      }
      
      if (i + 1 == length)
      {
        System.out.print("\n");
      }
      
    }
    
    for(int i = 0; i < length; ++i) // line three
    {
      if (i % 3 == 0)
      {
        System.out.print("/");
      }
      else if (i % 3 == 1)
      {
        System.out.print(" ");
      }
      else if (i % 3 == 2)
      {
        System.out.print("\\");
      }
      
      if (i + 1 == length)
      {
        System.out.print("\n");
      }
    
    }
  
  }
  
}