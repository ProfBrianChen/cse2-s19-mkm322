import java.util.Arrays;
import java.util.Scanner;

public class lab09{
	
	public static int[] getRandom(int size){
		
		int[] list = new int[size];
		for(int i = 0; i < size; i++){
			list[i] = (int) (Math.random() * size);
		}
		
		return list;
	}
	
	public static int[] getAscend(int size){
		
		int[] list = new int[size];
		for(int i = 0; i < size; i++){
			list[i] = (int) (Math.random() * size);
		}
		Arrays.sort(list);
		return list;
		
	}
	
	public static int linearSearch(int[] list, int indexVal){
		
		for(int i = 0; i < list.length; i++){
			if(list[i] == indexVal){
				return i;
				
			}
			
		}
		
		return -1;
		
	}
	
	public static int binarySearch(int[] list, int val){
		
		int start = 0;
		int end = list.length - 1;		
		
		while(true){
			
			int middle = (end + start) / 2;
			
			if(start > end){
				break;
			}
			
			if(val == list[middle]){
				return middle;
			}
			
			if(val > list[middle]){
				start = middle + 1;
			}
			else if(val < list[middle]){
				end = middle - 1;
				
			}
			
		}
		return -1;
		
	}
	
	public static void main(String[] args){
		Scanner myScan = new Scanner(System.in);
		
		System.out.println("What search would you like to perform?");
		System.out.print("\n\t \"linear\" or \"binary\" \n--->");
		
		int k = 0;
		String junk = "";
		while(true){
			if(myScan.hasNext("linear")){
				k = 0;
				break;
			}
			else if(myScan.hasNext("binary")){
				k = 1;
				break;
			}
			junk = myScan.nextLine();
			System.out.println("ERROR: Enter valid input!");
			System.out.println("\n\t \"linear\" or \"binary\"");
			
		}
		junk = myScan.nextLine();
		switch(k){
			
			case 0:
			
			while(true){
				System.out.print("What size array?\n--->");
				if(myScan.hasNextInt()){
					k = myScan.nextInt();
					break;
				}
				junk = myScan.nextLine();
				System.out.println("ERROR: Enter valid input!");
				System.out.println("\t \"linear\" or \"binary\"");
			}
			int [] b = getRandom(k);
			k = (int)(Math.random() * k);
			System.out.println("Random number index = " + k + "\nFound at index = " + linearSearch(b, k));
			System.out.print("Array = ");
			for(int i = 0; i < b.length; i++){
				System.out.print(b[i]);
				if(i != b.length - 1){
					System.out.print(", ");
				}
			}
			System.out.println();
			break;
			
			case 1:
			
			while(true){
				System.out.print("What size array?\n--->");
				if(myScan.hasNextInt()){
					k = myScan.nextInt();
					break;
				}
				junk = myScan.nextLine();
				System.out.println("ERROR: Enter valid input!");
				System.out.println("\t \"linear\" or \"binary\"");
			}
			int [] a = getAscend(k);
			k = (int)(Math.random() * k);
			System.out.println("Random number index = " + k + "\nFound at index = " + binarySearch(a, k));
			
			for(int i = 0; i < a.length; i++){
				System.out.print(a[i]);
				if(i != a.length - 1){
					System.out.print(", ");
				}
			}
			System.out.println();
			break;
			
			
			
		}
		
		
	}
	
	
}