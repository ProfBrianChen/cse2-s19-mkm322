// the purpose of this program is to convert meters to inches

// this import statment accesses the java library for use of Scanner code
import java.util.Scanner;

//  starts main code
public class Convert{
  
  // this is the main method and will be where I write the code
  public static void main(String args[]){

    // the scanner instance is created to allow for user input    
    Scanner myScanner = new Scanner( System.in );
    
    // the user is prompt with the amount of meters to convert
    System.out.print("Enter the distance in meters: ");
    
    // the user's input is stored into the double variable "meters"
    double meters = myScanner.nextDouble();
    
    // the conversion for meters --> inches is done such that the value is truncated after four decimals
    double inches = (int) (meters * 39.3700789 * 10000);
    
    // incremental operator to adjust decimal places
    inches /= 10000;
    
    // prints the final results
    System.out.println(meters + " meters is " + inches + " inches.");
    
  }
  
}