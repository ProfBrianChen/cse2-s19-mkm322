// this program calculates the volume of a rectangular prism with int

// import statement to obtain Scanner methods from library
import java.util.Scanner;

// main class for BoxVolume program
public class BoxVolume{
  
  //main method starts the block of code for the program
  public static void main(String args[]){
    
    // creates instance of scanner for side lengths
    Scanner myScanner = new Scanner( System.in );
    
    // prompts the user for side width and stores it as an int
    System.out.print("The width side of the box is: ");
    int width = myScanner.nextInt();
    
    // prompts the user for side length and stores it as an int
    System.out.print("The length of the box is: ");
    int length = myScanner.nextInt();
    
    // prompts the user for height and stores it as an int
    System.out.print("The height of the box is: ");
    int height = myScanner.nextInt();
    
    // calculates volume and stores it as an int
    int volume = width * length * height;
    
    // returns the result to the terminal
    System.out.println("\nThe volume inside the box is: " + volume);
    
  }
  
}