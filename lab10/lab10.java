// Maximillian Machado
// Brian Chen
// 4/18/2019

//------------------------------

//     :Program details:

//------------------------------

/*
The purpose of this program is to create
and format matrices to use matrix addition
*/

public class lab10{
	
	
	// first helper method that generates a matrix increasing row by row
	public static int[][] increasingMatrix(int width, int height, boolean format){
		
		// format true = row major matrix configuration
		// format false = column major matrix configuration

		if(format){ // row major
			
			int[][] matrix = new int[height][];
			int sequence = 1; // sequence technique
			
			// the sequence counter keeps track of entry values read row by row
			for(int i = 0; i < height; i++){
				matrix [i] = new int[width];
				for(int j = 0; j < width; j++){
					matrix [i][j] = sequence;
					sequence++;
				}
				
			}
			return matrix;			
		}
		else{ // column major
			
			int[][] matrix = new int[width][];
			int sequence = 1; // sequence technique
			int sum = 0; // sum technique
			
			// sum technique takes into account the values of all previous entries in rows below current row
			
			// sentinal variables swapped and unique seq and sum technique to create arrays
			for(int i = 0; i < width; i++){
				matrix [i] = new int[height];
				for(int j = 0; j < height; j++){
					matrix [i][j] = sequence + sum;
					sum += width;
				}
				sequence++;
				sum = 0;
				
			}
			
			return matrix;	
		}
	
	
	}
	
	// second helper method used to print matrices
	public static void printMatrix(int[][] matrix, boolean format){
		
		// matrix must have entries, if not return null
		if(matrix == null){System.out.println("the array was empty!");}
		else if(format){
		
		
			// for loop that prints individual entries if format is row major
			for(int i = 0; i < matrix.length; i++){
				System.out.print("[ ");
				for(int j = 0; j < matrix[0].length; j++){
					System.out.print(matrix[i][j] + " ");
				}
				System.out.println("]");
				
			}
		}
		else{
			
			
			// for loop that prints individual entries if format is column major
			for(int i = 0; i < matrix[0].length; i++){
				System.out.print("[ ");
				for(int j = 0; j < matrix.length; j++){
					System.out.print(matrix[j][i] + " ");
				}
				System.out.println("]");
				
			}
		
		}
		
	}
	
	
	// third helper method that translates column major matrices to row major
	public static int[][] translate(int[][] matrix){
		
		// slate is the temporary method that creates this transition 
		// slate is declared in this for loop
		int [][] slate = new int[matrix[0].length][];
		for(int row = 0; row < matrix[0].length; row++){
			slate[row] = new int[matrix.length];
			
		}	
		
		// intialized slate in this for loop.
		for(int row = 0; row < slate.length; row++){
			for(int col = 0; col < slate[row].length; col++){
				slate[row][col] = matrix[col][row];
			}
			
		}
		
		return slate;
	}
	
	// final helper method that creates matrix addition
	public static int[][] addMatrix(int[][] matrixA, boolean formatA, int[][] matrixB, boolean formatB){
		
		// k is a variable counter that is not needed but does keep 
		// track of what matrix was translated from column to row major
		int k = 0;
		
		// this pair of if-if else statements check if there is a needed transition
		if(formatA == false){
			matrixA = translate(matrixA);
			formatA = true;
			k++;
		}
		else if(formatB == false){
			matrixB = translate(matrixB);
			formatB = true;
			k--;
		}
		
		// the method checks to see if the matrices are equal in height and width
		if(matrixA.length != matrixB.length | matrixA[0].length != matrixB[0].length){
			System.out.println("Unable to add input matrices.");
			return null;
		}
		
		// the first matrix being added is printed along with additional info
		System.out.println("Adding two matrices.");
		printMatrix(matrixA, formatA);
		if(k == 1){
			System.out.println("Translating column major to row major input.");
		}
		
		System.out.println("plus");
		
		// the second matrix being added is printed along with additional info
		printMatrix(matrixB, formatB);
		if(k == -1){
			System.out.println("Translating column major to row major input.");
		}
		
		// height and width is declared and intialized for the operation
		int height = matrixA.length;
		int width = matrixA[0].length;
		
		// the sum of arrays is allocated space
		int[][] sum = new int[height][];
		
		// for loop that operates on each entry for addition
		for(int row = 0; row < height; row++){
			sum[row] = new int[width];
			for(int col = 0; col < width; col++){
				sum[row][col] = matrixA[row][col] + matrixB[row][col];
			}
			
		}
		
		// output is printed
		System.out.println("output:");
		printMatrix(sum, true);
		System.out.println();
		
		return sum;
		
	}
	
	public static void main(String[] args){
		
		// initialized and declared random parameters
		int heightOne = (int)(Math.random() * 4) + 2;
		int heightTwo = (int)(Math.random() * 4) + 2;
		int widthOne = (int)(Math.random() * 4) + 2;
		int widthTwo = (int)(Math.random() * 4) + 2;
		
		// generated all matrices according to constraints
		int [][] matrixA = increasingMatrix(widthOne, heightOne, true);
		int [][] matrixB = increasingMatrix(widthOne, heightOne, false);
		int [][] matrixC = increasingMatrix(widthTwo, heightTwo, true);
		
		// created matrices printed
		System.out.println("Generating a matrix with width "+ widthOne +" and height "+ heightOne +":");
		printMatrix(matrixA, true);
		System.out.println("Generating a matrix with width "+ widthOne +" and height "+ heightOne +":");
		printMatrix(matrixB, false);
		System.out.println("Generating a matrix with width "+ widthTwo +" and height "+ heightTwo +":");
		printMatrix(matrixC, true);

		// sum of "matrixA" to "matrixB" and "matrixB" to "MatrixC"
		addMatrix(matrixA, true, matrixB, false);
		addMatrix(matrixB, false, matrixC, true);
		
	}
	
	
}