// creates the class
public class PokerHandCheck{
  
  // starts main method
  public static void main(String[] args){
    
    // here are my 5 randomized variables
    int cardNumberOne = (int) (Math.random() * 52) + 1;
    int cardNumberTwo = (int) (Math.random() * 52) + 1;
    int cardNumberThree = (int) (Math.random() * 52) + 1;
    int cardNumberFour = (int) (Math.random() * 52) + 1;
    int cardNumberFive = (int) (Math.random() * 52) + 1;
    
    // here are the calculations I used to find to compare cards and see if there are special (e.i king, queen, jack, ace)
    int cardValueOne = cardNumberOne % 13;
    int cardValueTwo = cardNumberTwo % 13;
    int cardValueThree = cardNumberThree % 13;
    int cardValueFour = cardNumberFour % 13;
    int cardValueFive = cardNumberFive % 13;
    
    // I start making the strings that are printed in the console
    System.out.println("\nYour Random cards were:");
    
    System.out.print("\tthe ");
    
    // I use 5 switch statements to create the main strings in the program
    switch( cardValueOne ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValueOne);
    }
    
    // I also use 5 of these logical series to print the suite of the card
    if ( 13 >= cardNumberOne ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumberOne) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumberOne) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumberOne) {
      System.out.println(" of Spades");
    }
    
    System.out.print("\tthe ");
    
    switch( cardValueTwo ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValueTwo);
    }
    
    if ( 13 >= cardNumberTwo ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumberTwo) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumberTwo) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumberTwo) {
      System.out.println(" of Spades");
    }
    
    System.out.print("\tthe ");
    
    switch( cardValueThree ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValueThree);
    }
    
    if ( 13 >= cardNumberThree ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumberThree) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumberThree) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumberThree) {
      System.out.println(" of Spades");
    }
    
    System.out.print("\tthe ");
    
    switch( cardValueFour ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValueFour);
    }
    
    if ( 13 >= cardNumberFour ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumberFour) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumberFour) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumberFour) {
      System.out.println(" of Spades");
    }
    
    System.out.print("\tthe ");
    
    switch( cardValueFive ){
      case 0:
        System.out.print("King");
        break;
      
      case 1:
        System.out.print("Ace");
        break;
        
      case 11:
        System.out.print("Jack");
        break;
        
      case 12:
        System.out.print("Queen");
        break;
        
      default:
        System.out.print(cardValueFive);
    }
    
    if ( 13 >= cardNumberFive ) {
      System.out.println(" of Diamonds");
    }
    else if ( 26 >= cardNumberFive) {
      System.out.println(" of Clubs");
    }
    else if ( 39 >= cardNumberFive) {
      System.out.println(" of Hearts");
    }
    else if ( 52 >= cardNumberFive) {
      System.out.println(" of Spades");
    }
    
    // this part and onwards is for the tests to see what hand I have.
    
    System.out.print("\n");
    
    // these three variables act as a condition checker to see
    int handPair = 0;
    int hand3kind = 0;
    
    // the next 4 variables represent the first of five sets of comparison variables
    // these four check to see if the first card has any matches
    int firstValueOne = cardValueOne == cardValueTwo ? 1 : 0;
    int firstValueTwo = cardValueOne == cardValueThree ? 1 : 0; 
    int firstValueThree = cardValueOne == cardValueFour ? 1 : 0;
    int firstValueFour = cardValueOne == cardValueFive ? 1 : 0;
    
    // I sum up the comparisons to see how many matches I get
    int pairCounterOne = firstValueOne + firstValueTwo + firstValueThree + firstValueFour;
    
    // this switch will repreat a few times. It checks to see if there is a pair or 3 of a kind. 
    // each card will have its own version of this
    switch ( pairCounterOne ){
      case 1:
        handPair += 1;
        break;
        
      case 2:
        hand3kind = 1;
        break;
    }
    
    // this checks for matches with the second card, and so on so fourth
    int secondValueOne = cardValueTwo == cardValueOne ? 1 : 0;
    int secondValueTwo = cardValueTwo == cardValueThree ? 1 : 0;
    int secondValueThree = cardValueTwo == cardValueFour ? 1 : 0;
    int secondValueFour = cardValueTwo == cardValueFive ? 1 : 0;
    
    // again, sums up the comparisons to check for three of a kind or pair.
    // I will stop commenting on the repeatition, but for all the rest of the cards
    // they follow the formula
    int pairCounterTwo = secondValueOne + secondValueTwo + secondValueThree + secondValueFour;
    
    switch ( pairCounterTwo ){
      case 1:
        handPair += 1;
        break;
        
      case 2:
        hand3kind = 1;
        break;
    }
    
    int thirdValueOne = cardValueThree == cardValueOne ? 1 : 0;
    int thirdValueTwo = cardValueThree == cardValueTwo ? 1 : 0;
    int thirdValueThree = cardValueThree == cardValueFour ? 1 : 0;
    int thirdValueFour = cardValueThree == cardValueFive ? 1 : 0;
    
    int pairCounterThree = thirdValueOne + thirdValueTwo + thirdValueThree + thirdValueFour;
    
    switch ( pairCounterThree ){
      case 1:
        handPair += 1;
        break;
        
      case 2:
        hand3kind = 1;
        break;
    }
    
    int fourthValueOne = cardValueFour == cardValueOne ? 1 : 0;
    int fourthValueTwo = cardValueFour == cardValueTwo ? 1 : 0;
    int fourthValueThree = cardValueFour == cardValueThree ? 1 : 0;
    int fourthValueFour = cardValueFour == cardValueFive ? 1 : 0;
    
    int pairCounterFour = fourthValueOne + fourthValueTwo + fourthValueThree + fourthValueFour;
    
    switch ( pairCounterFour ){
      case 1:
        handPair += 1;
        break;
        
      case 2:
        hand3kind = 1;
        break;
    }
    
    int fifthValueOne = cardValueFive == cardValueOne ? 1 : 0;
    int fifthValueTwo = cardValueFive == cardValueTwo ? 1 : 0;
    int fifthValueThree = cardValueFive == cardValueThree ? 1 : 0;
    int fifthValueFour = cardValueFive == cardValueFour ? 1 : 0;
    
    int pairCounterFive = fifthValueOne + fifthValueTwo + fifthValueThree + fifthValueFour;
    
    switch ( pairCounterFive ){
      case 1:
        handPair += 1;
        break;
        
      case 2:
        hand3kind = 1;
        break;
    }
    
    // I divide the handPair counter to account for duplications when counting pairs
    handPair /= 2;
  
    // I finially print results of the hand.
    if (handPair + hand3kind == 0) {
      
      System.out.println("You have a high card hand!");
      
    }
    else if (handPair == 1 & hand3kind == 0) {
      
      System.out.println("You have a pair!");
        
    }
    else if (handPair > 1 & hand3kind == 0){
      
      System.out.println("You have two pairs!");
      
    }
    else if (hand3kind > 0){
      
      System.out.println("You have a three of a kind!");
      
    }
    
    System.out.print("\n");
    
  }
  
}