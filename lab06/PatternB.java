//Maximillian Machado
//Brian Chen
//3.8.2019
//lab 6

// 

import java.util.Scanner; // imports Scanner methods

public class PatternB{

  public static void main(String[] arg){ // starts main method
    
    Scanner myScan = new Scanner(System. in);
    int triLength = 0;
    System.out.print("Integer in range of [1 - 10] = ");
    while (true)
    {
      if (!myScan.hasNextInt()) // checks int condition
      {
        System.out.print("Error: Integer is an invalid input. \nIntger = "); // prompts user again
        String clearScanner = myScan.nextLine(); // clears scanner
        continue;
      }
      triLength = myScan.nextInt();
      if (0 > triLength | triLength > 10) {
        System.out.print("Error: Integer is not in range. \nInteger = "); // prompts user again
        continue;
      }
      else 
      {
        break; // if none of the conditions are caught then move through loop
      }
    }
        
    // This loop creates the structure
    for(int j = triLength; j > 0; j--) { // this structure determines the amount of lines
      for(int i = 1; i - j < 1; i++) { // this loop handles the pattern as well as the length of each line
        System.out.print(" " + i); // prints each part of the pattern
      }
      System.out.println(); // starts next line for the structure
    }
    
    
  }
  
}