//Maximillian Machado
//Brian Chen
//3.8.2019
//lab 6

// 

import java.util.Scanner; // imports Scanner methods

public class PatternA{

  public static void main(String[] arg){ // starts main method
    
    Scanner myScan = new Scanner(System. in);
    int triLength = 0;
    System.out.print("Integer in range of [1 - 10] = ");
    while (true)
    {
      if (!myScan.hasNextInt()) // checks int condition
      {
        System.out.print("Error: Integer is an invalid input. \nIntger = "); // prompts user again
        String clearScanner = myScan.nextLine(); // clears scanner
        continue;
      }
      triLength = myScan.nextInt();
      if (0 > triLength | triLength > 10) {
        System.out.print("Error: Integer is not in range. \nInteger = "); // prompts user again
        continue;
      }
      else 
      {
        break; // if none of the conditions are caught then move through loop
      }
    }
    
    // this nested for loop handles the actual creation of the structure
    for(int j = triLength + 1; j > 0; j--) { // outter loop handles how many lines will be created
      for(int i = 1; i + j < triLength + 2; i++) { // inner loop handles the length of each line
        System.out.print(" " + i);
      }
      System.out.println();
    }
    
    
  }
  
}