//Maximillian Machado
//Brian Chen
//3.8.2019
//lab 6

// 

import java.util.Scanner; // imports Scanner methods

public class PatternC{

  public static void main(String[] arg){ // starts main method
    
    Scanner myScan = new Scanner(System. in);
    int triLength = 0;
    System.out.print("Integer in range of [1 - 10] = ");
    while (true)
    {
      if (!myScan.hasNextInt()) // checks int condition
      {
        System.out.print("Error: Integer is an invalid input. \nIntger = "); // prompts user again
        String clearScanner = myScan.nextLine(); // clears scanner
        continue;
      }
      triLength = myScan.nextInt();
      if (0 > triLength | triLength > 10) {
        System.out.print("Error: Integer is not in range. \nInteger = "); // prompts user again
        continue;
      }
      else 
      {
        break; // if none of the conditions are caught then move through loop
      }
    }
    
    int c = 1; // constraint variable to account for the indent
    
    for(int i = triLength; i > 0; i--) { // outter loop determines amount of lines
      for(int j = triLength; j > 0; j--){ // inner loop for pattern
        if (j > c) { // this is to control when to ident
          System.out.print(" ");
        }
        else { // continues the pattern
          System.out.print(j);
        }
      }
      System.out.println(""); // starts next line of structure.
      c++; // increments indent variable
    }
    
  }
  
}