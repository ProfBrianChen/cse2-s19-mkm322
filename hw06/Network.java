//Maximillian Machado
//hw06

// This class creates a pattern that is characterized by user inputs.
// The pattern is an example of nested loops, and uses while loops
// when checking user inputs.

// Scanner library to take user inputs
import java.util.Scanner;

public class Network{
	
	
	public static void main(String[] args) {
		
		// initialized variables used in the constructing for loops an user inputs
		Scanner myScan = new Scanner( System.in );
		// an int variable that takes into acount on which lines will the horizontal edge appear
		int edgeLine = 0; 
		// a boolean variable used to see whether the edges will be single or double
		boolean doubleEdge = false;
		
		// here is a series of blocks of code that take STDIN for user
		// once height, width, size, and edgeLen are defined, the structure can be made
		
		System.out.print("Input your desired height: "); // prompts user for input
		while(!myScan.hasNextInt()) { // runs test to see if the user satisfies input
			System.out.println("   Error: please type in an integer.");
			System.out.print("Input your desired height: ");
			String clearScan = myScan.nextLine(); // dumpts invalid input
		}
		int height = myScan.nextInt(); // stores dsired valid input.
		
		System.out.print("Input your desired width: ");
		while(!myScan.hasNextInt()) {
			System.out.println("   Error: please type in an integer.");
			System.out.print("Input your desired width: ");
			String clearScan = myScan.nextLine();
		}
		int width = myScan.nextInt();
		
		System.out.print("Input your desired size: ");
		while(!myScan.hasNextInt()) {
			System.out.println("   Error: please type in an integer.");
			System.out.print("Input your desired size: ");
			String clearScan = myScan.nextLine();
		}
		int size = myScan.nextInt();
		
		System.out.print("Input your desired length: ");
		while(!myScan.hasNextInt()) {
			System.out.println("   Error: please type in an integer.");
			System.out.print("Input your desired length: ");
			String clearScan = myScan.nextLine();
		}
		int edgeLen = myScan.nextInt();
		
		// here is a test to see whether there will be double or single edge lines
		if (size % 2 == 1) { // if the size is odd...
			edgeLine = (int)((double) size / 2 + 0.5); // calculation to find where the edges will be relative to the squares
			doubleEdge = false; // then there is only one edge line
		}
		else if (size % 2 == 0) { // if the size is even...
			edgeLine = size / 2; // calculation to show where the first edge will be
			doubleEdge = true; // then there is are two edge lines
		}
		
		// here is the body of the code that deals with the construction of the pattern
		
		for (int i = 1; i <= height; i++) { // height dictates how mant lines will be printed
			
			// modulo used to break the patterns down into an interval [0,size + edgeLen)
			// there are two segments that make up he charcter progression
			// the first segment is the subinterval (0,size] which encompasses the squares' body
			// the second segment is the subinterval [0] U (size,size + edgeLen]
			
			int segHeight = i % (size + edgeLen); 
			
			for (int j = 1; j <= width; j++) { // width dictates how many characters per line
				
				int segWidth = j % (size + edgeLen); // similar to outside this for loop this provides the correct character when printing
				
				if (segWidth != 0 && segWidth <= size) { // this is what to print if not in the horizontal space of the edges
					if (segHeight == 1 || segHeight == size) { // checks to see if the line is on the corner of the square
						if (segWidth == 1 || segWidth == size) { // checks to see if the pattern has reached a corner
							System.out.print("#");
						}
						else { // if it is not a corner print the side
							System.out.print("-");
						}
					}
					else if (size > segHeight & segHeight > 1) { // this handles when the line is in the middle of the corner lines
						if (segWidth == 1 || segWidth == size) { // checks to see if at the side or not
							System.out.print("|");
						}
						else {
							System.out.print(" "); // if empty then print a space
						}
					}
					else { // this handles when the computer reaches the vertical edges
						if (segWidth == edgeLine) { // uses the previously calculated edgle line to determine when to print horizontal
							System.out.print("|");
						}
						else if (doubleEdge & segWidth == edgeLine + 1) {
							System.out.print("|");
						}
						else {
							System.out.print(" ");
						}
					}
				}
				else if (segWidth <= (size + edgeLen) || segWidth == 0) { // these series of if statements deal with horizontal edges
					if (segHeight == edgeLine) {
						System.out.print("-");
					}
					else if (doubleEdge & segHeight == edgeLine + 1) {
						System.out.print("-");
					}
					else {
						System.out.print(" ");
					}
				}
			}
			
			
			System.out.println();
		}
	
	
	
	}
}
					