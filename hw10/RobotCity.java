/*
Maximillian Machado
Professor Brian Chen
Due 4/30/2019

------------: Program Details :------------

This program generates a square city with random
count of blocks. The city undergoes a robat invasion,
which has robots moving east out of the city limits.


*/

public class RobotCity{
	
	// helper method that generates a square city
	public static int[][] buildCity(){
		
		// random integer is initialized and declared for size of city
		int size = (int)(Math.random() * 6) + 10;
		
		// column array with member arrays
		int[][] cityArray = new int[size][];
		
		for(int col = 0; col < size; col++){
			
			// arrays with size-rows are created
			cityArray[col] = new int[size];
			
			// random ints are made for each entry in the array
			for(int row = 0; row < size; row++){
				
				cityArray[col][row] = (int)(Math.random()*899)+100;
				
			}
			
		}
		
		// note that the member cityArray[0][0] is the south-west most point
		return cityArray;
		
	}
	
	// helper method to print matrix
	public static void display(int[][] cityArray){
		
		System.out.println("\n\n");
		
		// prints the grid system such that the north west most block
		// is in the north west corner
		for(int i = cityArray.length - 1; i >= 0; i--){
			
			for(int j = 0; j < cityArray.length; j++){
				
				System.out.printf("%4d  ", cityArray[j][i]);
				
			}
			
			System.out.println("\n");
			
		}
		
	}
	
	// helper method which places robots at random
	public static void invade(int[][] cityArray, int k){
		
		// for-loop generates robots
		for(int i = 0; i < k; i++){
			
			// two randomly generated cords on the grid
			int x = (int)(Math.random() * cityArray.length);
			int y = (int)(Math.random() * cityArray.length);
			
			// if a robot is already at the cords
			// then re-generate cords to place robot else where
			if(cityArray[x][y] < 0){
				i--;
				continue;
			}
			
			// create robot
			cityArray[x][y] = -1 * cityArray[x][y];
			
		}
		
	}
	
	// helper method to show movement of robots with each iteration
	public static void update(int[][] cityArray){
		
		// starts searching for robots at right most column
		for(int col = cityArray.length - 1; col >= 0; col--){
			
			// searches through each member in the member arrays
			for(int row = 0; row < cityArray.length; row++){
				
				if(cityArray[col][row] < 0){
					
					// when the robot is found, revert sign
					cityArray[col][row] = -1 * cityArray[col][row];
					
					// for the first column, make robot leave grid
					if(col == cityArray.length - 1){
						continue;
					}
				
					// any  other case, create new instance of robot
					cityArray[col + 1][row] = -1 * cityArray[col + 1][row];
				
				}
				
			}
			
		}
		
	}
	
	
	public static void main(String[] args){
		
		// create city and display it
		int[][] city = buildCity();
		display(city);
		
		// invade city with k = 5 robots and display it
		invade(city, 5);
		display(city);		
		
		// show robots' next 5 moves
		for(int i = 0; i < 4; i++){
			update(city);
			display(city);
		}
	}
}