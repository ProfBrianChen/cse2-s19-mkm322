/*
Maximillian Machado
Professor Brian Chen
Due 4/30/2019

------------: Program Details :------------

The purpose of this program is to create a deck 
of cards to simulate 1,000,000 hands of 5 cards.

!NOTE!
The probability calculated from this program WILL
be lower than the Wikipedia counter part, because
wikipedia considers wrap straights.

The value must be <0.3925% on average

*/

import java.util.Arrays;

public class Straight{
	
	// helper method that swaps all cards at random
	public static void shuffle(int[] deck){
		
		for(int i = 0; i < deck.length; i++){
			
			// random int is created to select which member to swap with
			int swap = (int) (Math.random() * 52);
			
			// swap technique
			int temp = deck[i];
			deck[i] = deck[swap];
			deck[swap] = temp;
			
		}
		
	}
	
	// helper method to remove top 5 cards
	public static int[] draw(int[] deck){
		
		int[] hand = new int[5];
		
		for(int i = 0; i < 5; i++){
			
			hand[i] = deck[i];
			
		}
		
		return hand;
		
	}
	
	// helper method to return card of asked rank (e.g 1 is lowest card; 5 is highest card)
	public static int getRank(int[] hand, int k){
		
		// if the card rank being asked is out of range, then print error, return -1 to exit method
		if(k > 5 | k < 0){
			System.out.println("ERROR: k is not a valid rank in deck!");
			return -1;
		}
		
		// sorted hand allows for index selection for rank
		Arrays.sort(hand);
		
		// k - 1, when array is sorted, will always be index of rank asked
		return hand[k - 1];
		
	}
	
	// helper method to check for straights in hand
	public static boolean straight(int[] hand){
		
		// counter keeps track of successful comparisons in hand
		// 1 comparison means 2 cards are in sequence
		// 4 cards means cards are a straight
		int counter = 0;
		
		for(int i = 0; i <hand.length - 1; i++){
			
			// this for-loop compares each card to see if 
			if(getRank(hand, i + 1) == getRank(hand, i + 2) - 1){
				counter++;
			}
			
			// counter is checked to see if any failures occur in comparisons
			// if a failure occurs, no point in running further calculations
			if(counter == i){
				break;
			}
			
		}

		// test: pass counter = 4, fail counter returns false
		if(counter == 4){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	public static void main(String[] args){
		
		// a sorted deck is created with 52 cards
		// cards are paired in intervals of 13
		int[] deck = new int[52];
		for(int i = 0; i < deck.length; i++){
			deck[i] = i;
		}
		
		// all card values are converted to their magnitude value [0,12]
		// card values with 0 = ace, card values with 12 = king
		for(int i = 0; i < deck.length; i++){
			deck[i] = deck[i] % 13;
		}
		
		// these counters will take into account
		// total amount of trials as well as 
		// successful straights
		double counter = 0.0;
		double success = 0.0;
		
		// 1,000,000 trials of shuffle and regathered hands
		for(int i = 0; i < 1000000; i++){
			
			shuffle(deck);
			int[] hand = draw(deck);
			
			if(straight(hand)){
				success = success + 1.0;
			}
			
			counter = counter + 1.0;
		}
		
		// success rate is calculated
		System.out.println(success / counter * 100 + "%");
		
	}
	
}