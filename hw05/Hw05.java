//Maximillian Machado
//Brian Chen
//3.2.2019
//hw 5

// the program checks to see if the user put in the correct input. if the input is
// of an unwanted type then the user is forced to in a new input.

import java.util.Scanner; // imports Scanner methods

public class Hw05{

  public static void main(String[] arg){ // starts main method
  
    Scanner myScan = new Scanner(System. in);
    
    //Structure:
    // users gives course number, 
    // department name, 
    // number of times meets in a week
    // the time the class starts,
    // the instructor name, 
    // the number of students
    
    // the user is given context and a guide of the program
    System.out.println("\nThe program will ask questions about a class you are in."); 
    System.out.println("If there is an error, please enter valid input.\n");
    System.out.print("Course number (digits only) = ");
    
    // this input checker is one of two structures I used
    // first input checking structure.
    while (!myScan.hasNextInt()) // checks scanner for non-int inputs
    {
      System.out.print("Error: Please give a valid integer!\nCourse number (digits only) = "); // sets up prompt again
      String errorCheck = myScan.nextLine(); // clears scanner
    }
    
    String clearString = myScan.nextLine(); // clears scanner
    
    System.out.print("Department name (1 word) = "); // next prompt
    
    Boolean checker = true; // I used this to create a while loop that can check int and double conditions
    
    // second input checking structure.
    while (checker)
    {
      if (myScan.hasNextInt()) // checks int condition
      {
        System.out.print("Error: Integer is an invalid input\nDepartment name (1 word) = "); // prompts user again
        int checkerOne = myScan.nextInt(); // clears scanner
      }
      else if (myScan.hasNextDouble()) // checks double condition
      {
        System.out.print("Error: Double is an invalid input\nDepartment name (1 word) = "); // prompts user again
        double checkerTwo = myScan.nextDouble(); // clears scanner
      }
      else 
      {
        checker = false; // if none of the conditions are caught then move through loop
      }

    }
    
    clearString = myScan.nextLine(); // clears scanner
    
    System.out.print("Times a week (digits only) = "); //prompts user
    
    // refer to the first input structure.
    while (!myScan.hasNextInt())
    {
      System.out.print("Error: Please give a valid integer!\nTimes a week (digits only) = ");
      String errorCheck = myScan.next();
    }
    
    clearString = myScan.nextLine();
    
    System.out.print("When is the class (XXXX AM/PM = XX:XX AM/PM) = "); // prompts user

    // refer to the first input structure.
    while (!myScan.hasNextInt())
    {
      System.out.print("Error: Please give a valid integer\nWhen is the class (XXXX AM/PM = XX:XX AM/PM) = ");
      String errorCheck = myScan.nextLine();
    }
    
    clearString = myScan.nextLine();

    System.out.print("Instructor Name (last name) = "); //prompts user
    
    checker = true;
    
    // second input structure.
    while (checker)
    {
      if (myScan.hasNextInt())
      {
        System.out.print("Error: Integer is an invalid input\nInstructor name (last name) = ");
        int checkerOne = myScan.nextInt();
      }
      else if (myScan.hasNextDouble())
      {
        System.out.print("Error: Double is an invalid input\nInstructor name (last name) = ");
        double checkerTwo = myScan.nextDouble();
      }
      else 
      {
        checker = false;
      }
      
      clearString = myScan.nextLine();
    }
    
    clearString = myScan.nextLine();
    
    System.out.print("How many students (digits only) = "); // prompts user
    
    // first input strucutre
    while (!myScan.hasNextInt())
    {
      System.out.print("Error: Please give a valid integer\nHow many students (digits only) = ");
      String errorCheck = myScan.next();
    }
    
  }
  
}